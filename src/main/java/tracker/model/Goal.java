package tracker.model;

/**
 * Created by evo on 11/20/2017.
 */
public class Goal {
    private int minutes;

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        this.minutes = minutes;
    }
}
