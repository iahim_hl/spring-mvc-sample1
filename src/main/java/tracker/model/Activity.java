package tracker.model;

/**
 * Created by evo on 11/20/2017.
 */
public class Activity {
    private String desc;

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
