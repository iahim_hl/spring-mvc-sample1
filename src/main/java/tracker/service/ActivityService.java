package tracker.service;

import org.springframework.stereotype.Service;
import tracker.model.Activity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by evo on 11/20/2017.
 */

@Service
public class ActivityService {

    public List<Activity> findAllActivities(){

        List<Activity> list = new ArrayList();
        Activity a1 = new Activity();
        a1.setDesc("Running");
        list.add(a1);

        Activity a2 = new Activity();
        a2.setDesc("Swiming");
        list.add(a2);

        Activity a3 = new Activity();
        a3.setDesc("Biking");
        list.add(a3);

        return list;
    }

}
