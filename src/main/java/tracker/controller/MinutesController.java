/*
 * Copyright 2012-2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package tracker.controller;

import java.util.*;
import java.util.Date;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import tracker.model.Activity;
import tracker.model.Exercise;
import tracker.service.ActivityService;

@Controller
public class MinutesController {

	@Value("${application.message:Hello World}")
	private String message = "Hello World";

    @Autowired
    ActivityService service;

	@RequestMapping("/welcome")
	public String welcome(Map<String, Object> model) {
		model.put("time", new Date());
		model.put("message", this.message);
		return "welcome";
	}

	@RequestMapping("/addMinutes")
	public String addExercise(@ModelAttribute("exercise") Exercise exercise) {
		System.out.println("Add minutes: "+exercise.getMinutes());
		return "addMinutes";
	}

	@RequestMapping(value="/activities", method = RequestMethod.GET)
	public @ResponseBody List<Activity> findAllActivities(){

		return service.findAllActivities();
	}




//	@RequestMapping("/addMoreMinutes")
//	public String addMoreExercise(@ModelAttribute("exercise") Exercise exercise) {
//		System.out.println("Add more minutes: "+exercise.getMinutes());
//		return "addMinutes";
//	}

	@RequestMapping("/foo")
	public String foo(Map<String, Object> model) {
		throw new RuntimeException("Foo");
	}

}
