<%--
  Created by IntelliJ IDEA.
  User: evo
  Date: 11/20/2017
  Time: 1:04 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html>
<head>
    <title>Add Goal</title>
</head>
<body>

<form:form commandName="goal">
    <table>
        <tr>
            <td>Enter Minutes:</td>
            <td><form:input path="minutes"></form:input></td>
        </tr>
        <tr>
            <td colspan="2"></td>
            <input type="submit" value="Enter Goal Minutes"/>
        </tr>
    </table>
</form:form>

<h1>Our goal for the day is: ${goal.minutes}</h1>

</body>
</html>
